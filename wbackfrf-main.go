package main

import (
	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/widget"
)

var (
	entry_bkp    *widget.Entry
	entry_html   *widget.Entry
	entry_feed   *widget.Entry
	start_button *widget.Button
)

var mwin fyne.Window

func main() {
	myApp := app.New()
	myWindow := myApp.NewWindow("BackFrf Lite - " + myversion)
	mwin = myWindow

	start_button = widget.NewButton("Start", func() {
		backup(entry_feed.Text)
	})
	buttons := container.NewGridWithColumns(4,
		widget.NewButton("Config", func() {
			configForm()
			//ReadConf()
		}),
		start_button,
	)
	label_feed := widget.NewLabel("Feed")
	entry_feed = widget.NewEntry()
	label_bkp := widget.NewLabel("Loaded")
	entry_bkp = widget.NewEntry()
	label_html := widget.NewLabel("Html pages")
	entry_html = widget.NewEntry()
	entry_bkp.Disable()
	entry_html.Disable()
	content := container.NewVBox(buttons, label_feed, entry_feed, label_bkp, entry_bkp, label_html, entry_html)
	myWindow.Resize(fyne.NewSize(640, 460))
	ReadConf()
	myWindow.SetContent(content)
	myWindow.ShowAndRun()
}
