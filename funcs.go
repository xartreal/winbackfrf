// service
package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	//	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/dialog"
)

func httprq(url string) []byte {
	var query = []byte("")
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(query))
	//	req.Header.Set("X-Authentication-Token", Config.myauth)
	//	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("User-Agent", useragent)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return []byte{0}
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

func httpget(url string) []byte {
	var query = []byte("")
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(query))
	req.Header.Set("X-Authentication-Token", Config.myauth)
	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("User-Agent", useragent)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
		return []byte{0}
	}
	defer resp.Body.Close()

	//fmt.Println("response Status:", resp.Status)
	//fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

func httpfile(url string) []byte {
	var query = []byte("")
	req, err := http.NewRequest("GET", url, bytes.NewBuffer(query))
	//	req.Header.Set("X-Authentication-Token", auth)
	req.Header.Set("User-Agent", useragent)
	//req.Header.Set("Content-Type", "application/json; charset=utf-8")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		println(err)
	}
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

func MkFeedPath(feedname string) {
	feedpath := ""
	if strings.Contains(feedname, "filter:") {
		feedpath = strings.Replace(feedname, ":", "/", -1)
	} else {
		feedpath = "feeds/" + feedname
	}
	RunCfg.feedpath = feedpath + "/"
}

func isexists(filename string) bool {
	_, err := os.Stat(filename)
	return err == nil
}

func outerror(code int, format string, a ...interface{}) {
	err := fmt.Errorf(format, a...)
	dialog.ShowError(err, mwin)
	//os.Exit(code)
}

func outinf(format string, a ...interface{}) {
	dialog.ShowInformation("BackFrf", fmt.Sprintf(format, a...), mwin)
	//os.Exit(code)
}
