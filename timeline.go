// timeline
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path"
	"regexp"
	"strconv"
	"strings"
)

var newitems []string

var nlflag bool

var extflag bool //true if medialfilter empty

func inmedialist(fext string) bool {
	if extflag {
		return true
	}
	if len(fext) == 0 {
		return false
	}
	lext := strings.Replace(fext, ".", "", 1)
	return strings.Contains(Config.filter, lext)
}

func getFile(id string, url string, media string, singlemode bool) {
	var mtype string
	fext := path.Ext(url)
	if strings.EqualFold(media, "image") {
		mtype = "image_"
	} else {
		mtype = "media_"
		if (!singlemode && Config.loadmedia != 1) || (Config.loadmedia == 1 && !inmedialist(fext)) {
			ioutil.WriteFile(RunCfg.feedpath+"media/"+mtype+id+fext, []byte("media file not loaded"), 0755)
			//fmt.Printf("m=%s e=%s\n", id, fext)
			ExtDB.MyCollection.Set([]byte(id), []byte(fext))
			MyStat.newimages++
			return
		}
	}
	if isexists(RunCfg.feedpath + "media/" + mtype + id + fext) {
		return
	} // if file exists

	ExtDB.MyCollection.Set([]byte(id), []byte(fext))

	body := httpfile(url)
	//fmt.Println("response Body:", string(body))
	ioutil.WriteFile(RunCfg.feedpath+"media/"+mtype+id+fext, body, 0755)
	MyStat.newimages++
}

func getPost(id string, singlemode bool) {
	url := "https://freefeed.net/v2/posts/" + id + "?maxComments=all&maxLikes=all"
	body := httpget(url)
	//fmt.Println("response Body:", string(body))
	//json correction
	rp := regexp.MustCompile(`"([a-zA-Z]{2,})":(\d{1,})`)
	outtext := rp.ReplaceAll(body, []byte(`"$1":"$2"`))

	ioutil.WriteFile(RunCfg.feedpath+"json/posts_"+id, outtext, 0755)
	// attach
	frfz := new(FrFfile)
	json.Unmarshal(outtext, frfz)
	for _, p := range frfz.Attachments {
		getFile(p.Id, p.Url, p.MediaType, singlemode)
	}
}

func getTimeline(offset int, feedname string) []byte {
	url := "https://freefeed.net/v2/timelines/" + feedname + "?offset=" + strconv.Itoa(offset)

	body := httpget(url)
	// json correction
	rp := regexp.MustCompile(`"([a-zA-Z]{2,})":(\d{1,})`)
	outtext := rp.ReplaceAll(body, []byte(`"$1":"$2"`))

	if ioutil.WriteFile(RunCfg.feedpath+"timeline/timeline_"+strconv.Itoa(offset), outtext, 0755) != nil {
		//	println("err in: " + RunCfg.feedpath + "timeline/timeline_" + strconv.Itoa(offset))
	}
	return outtext
}

func processTimeline(text []byte, offset int) int {
	frf := new(FrFjtml)
	//fmt.Printf("\roffset: %d ", offset)
	entry_bkp.SetText(fmt.Sprintf("offset: %d ", offset))
	tlist := ""
	nlflag = false
	json.Unmarshal(text, frf)
	frflen := 0
	//	fmt.Printf("t=%q\n",frf.Timelines.Posts)
	for idx, p := range frf.Timelines.Posts {
		if Config.archive != 1 && len(frf.Posts[idx].FriendfeedUrl) != 0 { //skip archive post
			continue
		}
		tlist += p + "\n"
		cx, _ := strconv.Atoi(frf.Posts[idx].OmittedComments)
		cmark := strconv.Itoa(len(frf.Posts[idx].Comments) + cx)
		//		fmt.Printf("p=%s,%s\nt=%q\n",idx,p,frf.Posts[idx])
		newmark := frf.Posts[idx].UpdatedAt + cmark
		frflen++
		if !isexists(RunCfg.feedpath + "json/posts_" + p) {
			MyStat.newrecords++
			newitems = append(newitems, p)
			TimelineDB.MyCollection.Set([]byte(p), []byte(newmark))
			getPost(p, false)
			nlflag = true
		} else {
			oldmark, _ := TimelineDB.MyCollection.Get([]byte(p))
			if !strings.EqualFold(string(oldmark), newmark) {
				TimelineDB.MyCollection.Set([]byte(p), []byte(newmark))
				getPost(p, false)
				MyStat.changedrecords++
				newitems = append(newitems, p)
				nlflag = true
			}
		}
	}
	if len(tlist) > 0 {
		ioutil.WriteFile(RunCfg.feedpath+"index/list_"+strconv.Itoa(offset), []byte(tlist), 0755)
	}
	MyStat.records += frflen
	return frflen
}
