// backup
package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"regexp"
	"strconv"
	"strings"
	"time"
)

var TimelineDB KVBase
var ExtDB KVBase

func whoami(auth string, singlemode bool) bool {
	url := "https://freefeed.net/v2/users/whoami"
	body := httpget(url)
	matched, err := regexp.MatchString("err\":", string(body))
	if matched { // auth error
		ioutil.WriteFile("error.json", body, 0755)
		return false
	}
	if !singlemode {
		if err = ioutil.WriteFile(RunCfg.feedpath+"json/profile", body, 0755); err != nil {
			outerror(1, "Error: %q\n", err)
		}
	}
	return true
}

func checkIsFeed(feedname string) bool {
	if strings.Contains(feedname, "filter:") || (feedname == "home") {
		return true
	}
	res := httprq("https://freefeed.net/v1/users/" + feedname)
	if strings.Contains(string(res), `{"err"`) {
		return false
	}
	//	fmt.Printf("%v\n", string(res))
	return strings.Contains(string(res), feedname)
}

func backup(feedname string) {
	tagReplacer = strings.NewReplacer("<", "&lt;", ">", "&gt;")

	if !checkIsFeed(feedname) {
		outerror(2, "ERROR: Feed '%s' not found or not available\n", feedname)
		return
	}
	MkFeedPath(feedname)
	feedname = strings.Replace(feedname, ":", "/", -1)
	// make structure
	if !isexists(RunCfg.feedpath) {
		//fmt.Printf("Creating directory for feed [%s]\n", feedname)
		if os.MkdirAll(RunCfg.feedpath+"html", 0755) != nil {
			outerror(1, " Can't create feed directory\n")
			return
		}
		os.MkdirAll(RunCfg.feedpath+"json", 0755)
		os.MkdirAll(RunCfg.feedpath+"db", 0755)
		os.MkdirAll(RunCfg.feedpath+"index", 0755)    //list_*
		os.MkdirAll(RunCfg.feedpath+"media", 0755)    // image_*, media_*
		os.MkdirAll(RunCfg.feedpath+"timeline", 0755) //timeline_*
	}
	// Check auth
	//	fmt.Print("Check token...")
	if !whoami(Config.myauth, false) {
		outerror(1, "Check token error!\n")
		return
	}
	//	fmt.Printf(" ok\n")

	// create timeline db (if not exists) & open
	dbname := RunCfg.feedpath + "db/timeline.db"
	if !isexists(dbname) {
		createDB(dbname, "posts", &TimelineDB)
	}
	openDB(dbname, "posts", &TimelineDB)
	// create ext db (if not exists) & open
	dbname = RunCfg.feedpath + "db/media.db"
	if !isexists(dbname) {
		createDB(dbname, "ext", &ExtDB)
	}
	openDB(dbname, "ext", &ExtDB)

	//	fmt.Printf("\n")

	MyStat.changedrecords = 0
	MyStat.newimages = 0
	MyStat.newrecords = 0
	MyStat.records = 0

	tmleof := 1
	tmlstat := ""
	extflag = len(Config.filter) == 0

	offset := 0
	for tmleof > 0 {
		if (Config.maxlast > 0) && (offset > Config.maxlast) {
			tmleof = 0
			continue
		}
		result := getTimeline(offset, feedname)
		if result[0] != 0 {
			tmleof = processTimeline(result, offset)
			if tmleof > 0 {
				tmlstat = tmlstat + "\n" + strconv.Itoa(tmleof)
			}
		} else {
			//			fmt.Printf(" -- timeout?\n\n")
		}
		offset += Config.step
	}

	// index json & make html
	rebuildHtml()

	// close timeline & media db
	closeDB(&ExtDB)
	closeDB(&TimelineDB)
	outstat := fmt.Sprintf("\nTotal: %d records, new: %d, changed: %d\n%d new media files\n",
		MyStat.records, MyStat.newrecords, MyStat.changedrecords, MyStat.newimages)
	timestat := time.Now().Format(time.RFC822)
	f, _ := os.OpenFile(RunCfg.feedpath+"stat.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	f.WriteString("\n" + timestat + "\n" + outstat)
	f.Close()
	outinf("Backup complete")
}

func rebuildHtml() {
	hstart := 0
	maxeof := Config.step
	for isexists(RunCfg.feedpath + "timeline/timeline_" + strconv.Itoa(hstart)) {
		hstart += Config.step
	}
	if hstart > Config.step {
		maxeof = hstart - Config.step
	}
	//	fmt.Printf(" %d records \n", maxeof-Config.step)
	//	fmt.Printf("HTMLing...\n")

	for i := 0; i < maxeof; i += Config.step {
		genhtml(i, maxeof)
	}
	if !isexists(RunCfg.feedpath + "html/kube.min.css") {
		fbin, _ := ioutil.ReadFile("template/kube.min.css")
		ioutil.WriteFile(RunCfg.feedpath+"html/kube.min.css", fbin, 0644)
	}
}
