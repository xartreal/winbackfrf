module bitbucket.org/xartreal/winbackfrf

go 1.16

require (
	fyne.io/fyne/v2 v2.0.3
	github.com/steveyen/gkvlite v0.0.0-20141117050110-5b47ed6d7458
	github.com/vaughan0/go-ini v0.0.0-20130923145212-a98ad7ee00ec
)
