// config
package main

import (
	"io/ioutil"
	"strconv"

	"github.com/vaughan0/go-ini"
)

var myversion = "0.1.3"
var useragent = "ARL backfrf-lite/" + myversion

var RunCfg struct {
	jsonmode bool
	jsononly bool
	feedpath string
}

var Config struct {
	myauth    string
	myname    string
	step      int
	logstat   int
	loadmedia int
	debugmode int
	allhtml   int
	maxlast   int
	archive   int
	filter    string
}

func getIniVar(file ini.File, section string, name string) string {
	rt, _ := file.Get(section, name)
	if len(rt) < 1 {
		//		outerror(2, "FATAL: Variable '%s' not defined\n", name)
	}
	return rt
}

func getIniNum(file ini.File, section string, name string) int {
	rt, err := strconv.Atoi(getIniVar(file, section, name))
	if err != nil {
		return 0
	}
	return rt
}

func WriteConf(name, token, filter string) {
	out := "[credentials]\r\n" +
		"auth=" + token + "\r\n" +
		"myname=" + name + "\r\n" +
		"[user]\r\n" +
		"filter=" + filter + "\r\n"
	ioutil.WriteFile("winbackfrf.ini", []byte(out), 0644)
}

func ReadConf() {
	file, err := ini.LoadFile("winbackfrf.ini")
	if err != nil {
		start_button.Disable()
		//		outerror(1, "\n! Configuration not found\n")
	}
	Config.myauth = getIniVar(file, "credentials", "auth")
	Config.myname = getIniVar(file, "credentials", "myname")

	Config.step = 30
	Config.debugmode = 0

	Config.logstat = 1
	Config.loadmedia = 1
	Config.allhtml = 0
	Config.maxlast = 0
	Config.archive = 0
	Config.filter, _ = file.Get("user", "filter")
	if len(Config.myauth) < 30 || len(Config.myname) < 3 {
		start_button.Disable()
	} else {
		start_button.Enable()
		start_button.Refresh()
	}
	entry_feed.SetText(Config.myname)
	//WrLog("ReadConf call\n")
}

func WrLog(text string) {
	fbin, _ := ioutil.ReadFile("mylog.log")
	fout := string(fbin) + text
	ioutil.WriteFile("mylog.log", []byte(fout), 0644)
}
