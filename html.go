// html
package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type GroupSType struct {
	Id          string
	IsPrivate   string
	IsProtected string
}

var tagReplacer *strings.Replacer

func striptags(text string) string {
	/*	outtext := strings.Replace(text, "<", "&lt;", -1)
		outtext = strings.Replace(outtext, ">", "&gt;", -1)
		return outtext*/
	outtext := tagReplacer.Replace(text)
	return strings.Replace(outtext, "\n", "<br>", -1)
}

func highlighter(text string) string {
	rx := regexp.MustCompile(`(?s)([^a-zA-Z0-9\/]|^)(@[a-zA-Z0-9\-]+)`)                                //@username
	ry := regexp.MustCompile(`(?s)([^a-zA-Z0-9\/?'"]|^)#([^\s\x20-\x2F\x3A-\x3F\x5B-\x5E\x7B-\xBF]+)`) //hashtag
	rz := regexp.MustCompile(`(?s)(https?:[^\s^\*(),\[{\]};'"><]+)`)                                   //url
	out := rx.ReplaceAll([]byte(text), []byte(`$1<span style="color:blue">$2</span>`))
	out = ry.ReplaceAll([]byte(out), []byte(`$1<span style="color:red">#$2</span>`))
	out = rz.ReplaceAll([]byte(out), []byte(`<a href="$1">$1</a>`))
	return string(out)
}

func getgroups(xxx []byte) map[string]GroupSType {
	var groups = map[string]GroupSType{}
	var tmpgroups = map[string]GroupSType{}
	ga := new(FrFsubscribers)
	err := json.Unmarshal(xxx, ga)
	if err != nil {
		//		fmt.Printf("subcribers warn: %q\n", err)
	}
	for _, p := range ga.Subscribers {
		if strings.EqualFold(p.Type, "group") {
			tmpgroups[p.Id] = GroupSType{p.Username, p.IsPrivate, p.IsProtected}
		}
	}
	gb := new(FrFsubscriptions)
	err = json.Unmarshal(xxx, gb)
	if err != nil {
		//		fmt.Printf("subscriptions warn: %q\n", err)
	}
	for _, p := range gb.Subscriptions {
		if tmpgroups[p.User].Id != "" {
			groups[p.Id] = GroupSType{tmpgroups[p.User].Id, tmpgroups[p.User].IsPrivate, tmpgroups[p.User].IsProtected}
		}
	}
	return groups
}

func tohtml(id string, jsndata []byte, singlemode bool) string {
	frf := new(FrFJSON)
	frfusr := new(FrFusers)
	frfcmts := new(FrFcomments)
	err := json.Unmarshal(jsndata, frf)
	if err != nil {
		/*		fmt.Printf("html warn: %s\n", id)
				fmt.Printf("jsn err: %q", err)*/
		return ""
	}
	json.Unmarshal([]byte(jsndata), frfusr)

	var usrindex = map[string]GroupSType{}

	//users
	for _, p := range frfusr.Users {
		//	fmt.Println(p.Id, p.Username)
		usrindex[p.Id] = GroupSType{p.Username, p.IsPrivate, p.IsProtected}
	}

	var privatetext string

	// groups
	groups := getgroups(jsndata)
	ghtml := ""
	var gcnt = 0
	var pscnt = len(frf.Posts.PostedTo)
	for _, p := range frf.Posts.PostedTo {
		if groups[p].Id != "" {
			if groups[p].IsPrivate != "1" {
				ghtml += groups[p].Id + ":"
			} else {
				ghtml += `<span style="color:red">` + groups[p].Id + `</span>:`
			}
			gcnt++
		}
	}
	if (pscnt > 1) && (pscnt > gcnt) {
		ghtml = "+" + ghtml
	}

	//fmt.Printf("Created by %s / ", usrindex[frf.Posts.CreatedBy])
	uuname := usrindex[frf.Posts.CreatedBy].Id
	if strings.EqualFold(usrindex[frf.Posts.CreatedBy].IsPrivate, "1") {
		uuname = `<span style="color:red">` + uuname + `</span>`
	} else if strings.EqualFold(usrindex[frf.Posts.CreatedBy].IsProtected, "1") {
		uuname = `<span style="color:goldenrod">` + uuname + `</span>`
	} else {
		uuname = `<span style="color:green">` + uuname + `</span>`
	}
	//auname := ghtml + uuname
	utime, _ := strconv.ParseInt(frf.Posts.CreatedAt, 10, 64)
	tplFile, _ := ioutil.ReadFile("template/template_elemt.html")
	tplText := string(tplFile)
	tplText = strings.Replace(tplText, "$auname", ghtml+uuname, -1)
	tplText = strings.Replace(tplText, "$id", id, -1)
	tplText = strings.Replace(tplText, "$time_html", time.Unix(utime/1000, 0).Format(time.RFC822), -1)
	//likes
	likesHtml := "<p>Likes: "
	if len(frf.Posts.Likes) == 0 {
		likesHtml = ""
	} else {
		for _, p := range frf.Posts.Likes {
			likesHtml += usrindex[p].Id + ", "
			//fmt.Printf("%s ", usrindex[p])
		}
		likesHtml += "</p>\n"
	}
	//attach
	attachHtml := ""
	folder := "media/"
	prefix := "../media/"
	if singlemode {
		folder = "/"
		prefix = ""
	}
	for _, p := range frf.Posts.Attachments {
		fextb, _ := ExtDB.MyCollection.Get([]byte(p))
		fext := string(fextb)
		file := p + fext
		//fmt.Printf("path=%s\n", RunCfg.feedpath+folder+"image_"+file)
		if !isexists(RunCfg.feedpath + folder + "image_" + file) {
			attachHtml += "<a href=" + prefix + "media_" + file + "> Media file </a><br>\n"
		} else {
			imgsrc := prefix + `image_` + file
			attachHtml += `<a href="` + imgsrc + `">` + `<img width=233 height=175 src="` + imgsrc + `"></a><br>` + "\n"
		}
	}
	//comments
	json.Unmarshal([]byte(jsndata), frfcmts)
	commHtml := "<ul>"
	for _, p := range frfcmts.Comments {
		clikes := ""
		if p.Likes != "0" {
			clikes = ` <sup style="color:#FF8C00; font-weight:bold">` + p.Likes + "</sup>"
		}
		commHtml += "<li>" + highlighter(striptags(p.Body)) + " - <i>" + usrindex[p.CreatedBy].Id + "</i>" + clikes + "\n"
	}
	commHtml += "</ul>"
	//assembly
	tpliFile, _ := ioutil.ReadFile("template/template_item.html")
	outText := string(tpliFile)
	outText = strings.Replace(outText, "$private", privatetext, -1)
	outText = strings.Replace(outText, "$text", highlighter(striptags(frf.Posts.Body)), -1)
	outText = strings.Replace(outText, "$time_html", tplText, -1)
	outText = strings.Replace(outText, "$attach_html", attachHtml, -1)
	outText = strings.Replace(outText, "$likes_html", likesHtml, -1)
	outText = strings.Replace(outText, "$comm_html", commHtml, -1)
	return outText
}

func mkhtml(id string, htmlText string, isIndex bool, maxeof int) {
	tplFile, _ := ioutil.ReadFile("template/template_file.html")
	outfiletext := strings.Replace(string(tplFile), "$html_text", htmlText, -1)
	outname := id + ".html"
	if !isIndex {
		outname = "html/" + outname
		outfiletext = strings.Replace(outfiletext, "$pager", "", -1)
	} else {
		outname = "html/index_" + outname
		nav := `<nav class="pager">`
		ids, _ := strconv.Atoi(id)
		if ids != 0 {
			nav += "<a href=index_" + strconv.Itoa(ids-Config.step) + `.html class="is-prev">Previous</a>`
		}
		if ids < maxeof {
			nav += "<a href=index_" + strconv.Itoa(ids+Config.step) + `.html class="is-next">Next</a>`
		}
		nav += "</nav>"
		outfiletext = strings.Replace(outfiletext, "$pager", nav, -1)
	}
	//fmt.Printf("indx %s", outname)
	ioutil.WriteFile(RunCfg.feedpath+outname, []byte(outfiletext), 0755)
}

func genhtml(offset int, maxeof int) {
	data, _ := ioutil.ReadFile(RunCfg.feedpath + "index/list_" + strconv.Itoa(offset))
	llist := strings.Split(string(data), "\n")
	maxx := len(llist) - 1
	iitext := ""
	for i := 0; i < maxx; i++ {
		jdata, _ := ioutil.ReadFile(RunCfg.feedpath + "json/posts_" + llist[i])
		outtext := tohtml(llist[i], jdata, false)
		iitext += outtext + "<hr>"
		if Config.allhtml == 1 {
			mkhtml(llist[i], outtext, false, maxeof)
		}
	}
	mkhtml(strconv.Itoa(offset), iitext, true, maxeof)
	fmt.Printf("\roffset %d done", offset)
	entry_html.SetText(fmt.Sprintf("offset %d", offset))
}
