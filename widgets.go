// widgets
package main

import (
	"fmt"

	"fyne.io/fyne/v2"
	//	"fyne.io/fyne/v2/app"
	//	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/validation"
	"fyne.io/fyne/v2/widget"
)

func configForm() {
	WrLog("configForm call\n")
	w := fyne.CurrentApp().NewWindow("Config")
	name := widget.NewEntry()
	name.SetPlaceHolder("username")
	name.SetText(Config.myname)
	name.Validator = validation.NewRegexp(`\w{3,}`, "not a valid username")
	token := widget.NewEntry()
	token.SetPlaceHolder("126Hd9cPgC6S5918FA43rX3H6OiZXBs833oB2hgUPdN2OxujMUiLwa5m9R1ti721D4v51OPOM0DCo8SR5T0Xw3ua7Dtt52oRBRCaoX9zBybYVLb2x77hmiCCv3Hyaj3KDMyx8t")
	token.SetText(Config.myauth)
	token.Validator = validation.NewRegexp(`\w{30,}`, "not a valid token")

	filter := widget.NewEntry()
	filter.SetPlaceHolder("mp3,pdf")
	filter.SetText(Config.filter)

	form := &widget.Form{
		Items: []*widget.FormItem{
			{Text: "Name", Widget: name, HintText: "Your username"},
			{Text: "Token", Widget: token, HintText: "A valid token"},
			{Text: "Media filter", Widget: filter, HintText: "File extensions"},
		},
		OnCancel: func() {
			fmt.Println("Cancelled")
			w.Close()
		},
		OnSubmit: func() {
			fmt.Println("Form submitted")
			WriteConf(name.Text, token.Text, filter.Text)
			ReadConf()
			w.Close()
			/*			fyne.CurrentApp().SendNotification(&fyne.Notification{
						Title:   "Form for: " + name.Text,
						Content: largeText.Text,
					})*/
		},
	}

	w.SetContent(form)
	w.CenterOnScreen()
	w.Resize(fyne.NewSize(640, 480))
	w.Show()

}
